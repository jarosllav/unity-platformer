using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Player))]
public class PlayerEditor : Editor {
    public override void OnInspectorGUI() {
        Player player = (Player)target;
        DrawDefaultInspector();

        if(GUILayout.Button("Hit")) { 
            player.Hit(); 
        }
        else if(GUILayout.Button("Die")) { 
            player.Hit(player.GetHealth());
            player.Die(); 
        }
    }
}