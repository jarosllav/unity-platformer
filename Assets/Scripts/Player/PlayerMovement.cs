using System;
using UnityEngine;

public class PlayerMovement {
    private Settings settings;
    private Rigidbody2D rigidbody;
    private Transform groundCheck;

    private bool isGrounded = false;
    private bool isLanded = false;
    private bool isStopped = false;
    private bool isDoubleJumped = false;

    private float horizontalInput;

    public PlayerMovement(Settings settings) {
        this.settings = settings;
    }

    // Public methods
    public void Update() {
        if(this.isLanded)
            this.isLanded = false;
        
        if(this.isStopped)
            return;

        Vector2 velocity = new Vector2(this.horizontalInput, this.rigidbody.velocity.y);
        velocity.x = Mathf.MoveTowards(this.rigidbody.velocity.x, velocity.x, 100 * Time.deltaTime);

        this.rigidbody.velocity = velocity;

        bool wasGrounded = this.isGrounded;
        this.checkGround();

        if(!wasGrounded && this.isGrounded) {
            this.isLanded = true;
        }
    }

    public void Move(float input) {
        this.horizontalInput = input * this.settings.moveSpeed;
    }

    public void Jump() {
        if(!this.isDoubleJumped) {
            this.isDoubleJumped = true;
            this.rigidbody.velocity = new Vector2(this.rigidbody.velocity.x, 0f);
        }

        this.rigidbody.AddForce(Vector2.up * this.settings.jumpForce, ForceMode2D.Impulse);
    }

    public void Impulse(Vector2 force) {
        this.horizontalInput = force.x;
        this.rigidbody.AddForce(force, ForceMode2D.Impulse);
    }

    public void Stop() { 
        this.rigidbody.velocity = Vector2.zero;
        this.isStopped = true;
    }

    public void SetGroundCheck(Transform transform) {
        this.groundCheck = transform;
    }
    public void SetRigidbody(Rigidbody2D rigidbody) {
        this.rigidbody = rigidbody;
    }

    public bool CanDoubleJump() {
        return !this.isDoubleJumped;
    }

    public bool IsGrounded() {
        return this.isGrounded;
    }
    public bool IsFalling() {
        return this.rigidbody.velocity.y < 0f;
    }
    public bool IsLanded() {
        return this.isLanded;
    }

    public int GetDirection() {
        return (int)Mathf.Sign(this.horizontalInput);
    }

    // Private methods
    private void checkGround() {
        Collider2D collider = Physics2D.OverlapCircle(this.groundCheck.position, this.settings.groundCheckRadius, this.settings.groundMask);
        if(collider != null && !collider.isTrigger) {
            this.isGrounded = true;
            this.isDoubleJumped = false;
        }
        else {
            this.isGrounded = false;
        }
    }

    [Serializable]
    public class Settings {
        public float moveSpeed;
        public float jumpForce;
        public float groundCheckRadius = 0.15f;
        public LayerMask groundMask;
    }
}