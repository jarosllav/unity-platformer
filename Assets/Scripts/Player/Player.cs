using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;

public class Player : MonoBehaviour
{
    public static event Action<int> onCollectCoin;
    public static event Action<int> onHit;
    public static event Action<int, int> onChangeHealth;
    public static event Action onDie;

    public bool EnableDoubleJump = true;

    [SerializeField] private Transform groundCheck;

    private Settings settings;
    private PlayerMovement movement;
    private PlayerAnimator animator;

    private int coinsCount = 0;
    private int health = 0;

    private float hitTime = 0f;

    private bool isHitted = false;
    private bool isAttacking = false;
    private bool isLanding = false;

    [Inject]
    private void Contruct(Settings settings, PlayerMovement movement, PlayerAnimator animator) {
        this.settings = settings;
        this.movement = movement;
        this.animator = animator;

        this.movement.SetGroundCheck(groundCheck);
        this.movement.SetRigidbody(GetComponent<Rigidbody2D>());
        this.animator.SetSpriteRenderer(GetComponent<SpriteRenderer>());
        this.animator.SetAnimator(GetComponent<Animator>()); // yeahh
        this.health = settings.lives;
    }

    private void Awake() {
        // ...
    }

    private void Update() {
        if(this.health > 0)
            this.handleInput();

        if(this.isHitted)
            this.updateHitTime();
    }

    private void FixedUpdate() {
        this.movement.Update();
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        if(collider.tag == "Coin") {
            this.collectCoin(collider.GetComponent<Coin>());
        }
        else if(collider.tag == "Apple") {
            this.collectApple(collider.gameObject);
        }
        else if(collider.tag == "InfoPlate") {
            collider.GetComponent<InfoPlate>().Show();
        }
    }

    private void OnTriggerStay2D(Collider2D collider) {
        if(collider.tag == "Enemy") {
            this.HitByEnemy(collider.GetComponent<Enemy>());
        }
    }

    // Public methods
    public void Hit(int strength = 1) {
        this.SetHealth(this.health - strength);
        Player.onHit?.Invoke(this.health);

        if(this.health <= 0) {
            this.Die();
        }

        this.hitTime = this.settings.hitImmortalTime;
        this.isHitted = true;
    }

    public void HitByEnemy(Enemy enemy) {
        if(!this.isHitted && !enemy.IsHitted() && !enemy.IsDead()) {
            this.Hit(1);

            if(!this.IsDead()) {
                Vector2 force = this.settings.hitForce;
                force.x *= -1f * this.animator.GetDirection();

                this.movement.Impulse(force);   
            }
        }
    }

    public void Die() {
        this.animator.PlayDie();
        this.movement.Stop();
        Player.onDie?.Invoke();
    }

    public void Attack() {
        this.isAttacking = true;
        this.animator.PlayAttack();

        Vector2 direction = Vector2.right * this.animator.GetDirection();

        RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, this.settings.attackRange, this.settings.attackMask);
        if(hit) 
            this.handleHittedTarget(hit.collider.gameObject);
    }

    public void SetHealth(int health) {
        Player.onChangeHealth?.Invoke(health, this.health);
        this.health = health;
    }

    public int GetHealth() {
        return this.health;
    }

    public bool IsDead() {
        return this.health <= 0;
    }

    // Private methods
    private void handleInput() {
        float horizontal = Input.GetAxisRaw("Horizontal");

        if(!this.isHitted)
            this.movement.Move(horizontal);

        if(Input.GetButtonDown("Jump")) {
            if(this.movement.IsGrounded() || (this.EnableDoubleJump && this.movement.CanDoubleJump())) {
                this.movement.Jump();

                if(!isAttacking) {
                    this.animator.PlayLand();
                    this.isLanding = true;
                }
            }
        }

        if(Input.GetButtonUp("Attack")) { 
            if(!this.isAttacking) {
                this.Attack();
                this.isLanding = false;
            }
        }

        this.updateAnimatorByInput(horizontal);
    }

    private void updateAnimatorByInput(float horizontal) {
        if(this.isAttacking || this.isLanding)
            return;

        if(this.movement.IsGrounded()) {
            if(this.movement.IsLanded()) {
                this.animator.PlayLand();
                this.isLanding = true;
            }
            else {
                if(horizontal == 0f){
                    this.animator.PlayIdle();
                }
                else {
                    this.animator.PlayRun();
                }
            }
        } 
        else {
            if(this.movement.IsFalling()) {
                this.animator.PlayFall();
            }
            else {
                this.animator.PlayJump();
            }
        }

        if(horizontal != 0 && (horizontal < 0) != this.animator.IsFlipped())
            this.animator.Flip();
    }

    private void updateHitTime() {
        this.hitTime -= Time.deltaTime;
        if(this.hitTime <= 0) {
            this.isHitted = false;
        }
    }

    private void collectCoin(Coin coin) {
        coin.Pickup();
        this.coinsCount += 1;
        Player.onCollectCoin?.Invoke(this.coinsCount);
    }

    private void collectApple(GameObject apple) {
        int health = this.health + 1;
        if(health > this.settings.lives)
            health = this.settings.lives;

        this.SetHealth(health);
        Destroy(apple);
    }

    private void handleHittedTarget(GameObject gameObject) {
        if(gameObject.tag == "Enemy") {
            Enemy enemy = gameObject.GetComponent<Enemy>();
            enemy?.Hit(this.settings.attackStrength);
        }
        else if(gameObject.tag == "LootBox") {
            LootBox box = gameObject.GetComponent<LootBox>();
            box?.Open();
        }
    }

    private void Ani_AttackComplete() {
        this.isAttacking = false;
    }
    private void Ani_JumpTransition() {
        this.isLanding = false;
    }

    [Serializable]
    public class Settings {
        public int lives = 3;
        public float hitImmortalTime = 0.5f;
        public Vector2 hitForce = new Vector2(7, 4);
        public LayerMask attackMask;
        public float attackRange = 1f;
        public int attackStrength = 1;
    }
}
