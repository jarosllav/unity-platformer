using System;
using UnityEngine;

public class PlayerAnimator {
    private Settings settings;

    private SpriteRenderer renderer;
    private Animator animator;
    private string priorityAnimation;

    public PlayerAnimator(Settings settings) {
        this.settings = settings;
    }

    public void SetSpriteRenderer(SpriteRenderer renderer) {
        this.renderer = renderer;
    }
    public void SetAnimator(Animator animator) {
        this.animator = animator;
    }

    public void Flip() {
        this.renderer.flipX = !this.renderer.flipX;
    }

    public bool IsFlipped() {
        return this.renderer.flipX;
    }

    public int GetDirection() {
        return this.renderer.flipX ? -1 : 1;
    }

    public void Play(string animation) {
        this.animator.Play(animation);
    }

    public void PlayIdle() => this.Play(this.settings.aniIdle);
    public void PlayRun() => this.Play(this.settings.aniRun);
    public void PlayJump() => this.Play(this.settings.aniJump);
    public void PlayFall() => this.Play(this.settings.aniFall);
    public void PlayLand() => this.Play(this.settings.aniLand);
    public void PlayDie() => this.Play(this.settings.aniDie);
    public void PlayAttack() => this.Play(this.settings.aniAttack);

    [Serializable]
    public class Settings {
        public string aniIdle = "Idle";
        public string aniRun = "Run";
        public string aniJump = "JumpUp";
        public string aniFall = "JumpDown";
        public string aniLand = "JumpTransition";
        public string aniDie = "Death";
        public string aniAttack = "Attack";
    }
}