using UnityEngine;

public class GUIInfoPlate : GUIMenu {
    [SerializeField] private TMPro.TextMeshProUGUI headerText;
    [SerializeField] private TMPro.TextMeshProUGUI infoText;
    [SerializeField] private Vector3 offset;

    private void Awake() {
        InfoPlate.onTrigger += this.onTrigger;
        this.Hide();
    }

    private void onTrigger(InfoPlate plate) {
        this.headerText?.SetText(plate.GetHeaderText());
        this.infoText?.SetText(plate.GetInfoText());
        
        transform.position = plate.transform.position + this.offset;

        this.Show();
    }
}