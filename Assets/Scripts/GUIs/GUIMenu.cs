using UnityEngine;

public class GUIMenu : MonoBehaviour {
    [ContextMenu("Show")]
    public virtual void Show() {
        for(int i = 0; i < transform.childCount; ++i)
            transform.GetChild(i).gameObject.SetActive(true);
    }
    
    [ContextMenu("Hide")]
    public virtual void Hide() {
        for(int i = 0; i < transform.childCount; ++i)
            transform.GetChild(i).gameObject.SetActive(false);
    }   
}