using UnityEngine;

public class GUICoinCounter : GUIMenu {
    [SerializeField] private TMPro.TextMeshProUGUI valueText;

    private void Awake() {
        Player.onCollectCoin += this.onCollectCoin;
    }

    private void onCollectCoin(int count) {
        this.valueText?.SetText(count.ToString());
    }
}