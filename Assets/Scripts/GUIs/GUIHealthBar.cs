using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GUIHealthBar : GUIMenu {
    [SerializeField] private GameObject iconPrefab;
    private List<GUIHealthIcon> icons = new List<GUIHealthIcon>(); 

    [Inject]
    public void Contruct(Player.Settings playerSettings) {
        for(int i = 0; i < playerSettings.lives; ++i) {
            this.icons.Add(this.createIcon());
        }
    }

    private void Awake() {
        Player.onChangeHealth += this.onChangeHealth;
    }

    private GUIHealthIcon createIcon() {
        GameObject iconObject = GameObject.Instantiate(iconPrefab, Vector3.zero, Quaternion.identity);
        iconObject.transform.parent = transform;
        return iconObject.GetComponent<GUIHealthIcon>();
    }

    private void resetIcons() {
        foreach(GUIHealthIcon icon in this.icons) {
            icon.ShowForeground();
        }
    }

    private void onChangeHealth(int health, int previousHealth) {
        for(int i = this.icons.Count - 1; i >= 0; --i) {
            if(i >= health) {
                this.icons[i].HideForeground();
            }
            else {
                this.icons[i].ShowForeground();
            }
        }
    }
}