using UnityEngine;

public class GUIHealthIcon : MonoBehaviour {
    [SerializeField] private GameObject foreground;
    [SerializeField] private GameObject background;
    
    public void HideForeground() {
        foreground?.SetActive(false);
    }

    public void ShowForeground() {
        foreground?.SetActive(true);
    }

    public bool IsForegroundActive() {
        return foreground.activeSelf;
    }
}