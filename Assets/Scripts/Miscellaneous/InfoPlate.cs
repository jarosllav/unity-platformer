using UnityEngine;
using System;

public class InfoPlate : MonoBehaviour
{
    public static event Action<InfoPlate> onTrigger;
    
    [SerializeField] private string headerText;
    [SerializeField, Multiline] private string infoText;

    public string GetInfoText() {
        return this.infoText;
    }

    public string GetHeaderText() {
        return this.headerText;
    }

    public void Show() {
        InfoPlate.onTrigger?.Invoke(this);
    }
}
