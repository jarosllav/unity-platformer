using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootBox : MonoBehaviour
{
    [SerializeField] private GameObject[] loot; 

    private Animator animator;

    private void Awake() {
        this.animator = GetComponent<Animator>();
    }

    public void Open() {
        this.animator.Play("Open");
    }

    private void Ani_Open() {
        foreach(GameObject prefab in this.loot) {
            Instantiate(prefab, transform.position + Vector3.up * 0.25f, Quaternion.identity);
        }
    }
}
