using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    private Animator animator;

    private void Awake() {
        this.animator = GetComponent<Animator>();
    }

    public void Pickup() {
        this.animator?.Play("Pickup");
        GetComponent<BoxCollider2D>().enabled = false;
    }

    private void Ani_Pickup() {
        Destroy(gameObject);
    }
}
