using System;
using UnityEngine;
using Zenject;

public class FollowCamera : MonoBehaviour
{
    [SerializeField] private Transform target;

    private Settings settings;
    private Camera camera;

    private Vector3 velocity = Vector3.zero;

    [Inject]
    public void Contruct(Settings settings, Camera camera) {
        this.settings = settings;
        this.camera = camera;
    }

    private void FixedUpdate() {
        if(this.target == null)
            return;

        Vector3 position = this.target.position + this.settings.offset;
        position.z = this.camera.transform.position.z;

        if(Mathf.Abs(transform.position.x - position.x) > this.settings.margin.x) 
            position.x = Mathf.Lerp(transform.position.x, position.x, this.settings.damping * Time.deltaTime);
        
        if(Mathf.Abs(transform.position.y - position.y) > this.settings.margin.y) 
            position.y = Mathf.Lerp(transform.position.y, position.y, this.settings.damping * Time.deltaTime);

        transform.position = position;
    }

    [Serializable]
    public class Settings {
        public Vector3 offset;
        public Vector2 margin;
        public float damping = 0.15f;
    }
}
