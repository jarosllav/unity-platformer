using UnityEngine;
using Zenject;

//[CreateAssetMenu(fileName = "ScriptableGameInstaller", menuName = "Installers/ScriptableGameInstaller")]
public class ScriptableGameInstaller : ScriptableObjectInstaller<ScriptableGameInstaller>
{
    public PlayerMovement.Settings movementSettings;
    public PlayerAnimator.Settings animatorSettings;
    public Player.Settings playerSettings;
    public FollowCamera.Settings cameraSettings;

    public override void InstallBindings() {
        Container.BindInstance(this.movementSettings);
        Container.BindInstance(this.animatorSettings);
        Container.BindInstance(this.playerSettings);
        Container.BindInstance(this.cameraSettings);
    }
}