using UnityEngine;
using Zenject;
using System;

public class GameInstaller : MonoInstaller
{
    private Settings settings;

    public override void InstallBindings() {
        this.installPlayer();

        Container.Bind<Camera>().FromInstance(Camera.main);
    }

    private void installPlayer() {
        Container.BindInterfacesAndSelfTo<PlayerMovement>().AsSingle();
        Container.BindInterfacesAndSelfTo<PlayerAnimator>().AsSingle();
        Container.Bind<Player>().FromComponentInHierarchy().AsSingle();
    }

    [Serializable]
    public class Settings {
        public GameObject playerPrefab;
    }
}