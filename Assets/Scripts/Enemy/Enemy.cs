using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public enum EnemyBehaviour {NEUTRAL, AGGRESSIVE}

public class Enemy : MonoBehaviour
{
    [SerializeField] private Transform[] patrolPoints;
    [SerializeField] private float speed = 5f;
    [SerializeField] private int maxHealth = 2;
    [SerializeField] private EnemyBehaviour behaviour = EnemyBehaviour.NEUTRAL;
    [SerializeField] private float attackRange = 0.2f;
    [SerializeField] private float attackSpeed = 1f;
    [SerializeField] private int attackDamage = 1;

    private Player player;
    private Animator animator;
    private SpriteRenderer spriteRenderer;

    private int health;
    private int targetPoint = 0;
    private int targetDirection = 1;

    private float attackTime = 0f;

    private bool isHitted = false;
    private bool isAttacking = false;

    [Inject]
    public void Contruct(Player player) {
        this.player = player;
    }

    private void Awake() {
        this.animator = GetComponent<Animator>();
        this.spriteRenderer = GetComponent<SpriteRenderer>();

        if(this.patrolPoints.Length >= 1) {
            transform.position = this.patrolPoints[0].position;
        }

        this.health = this.maxHealth;
    }

    private void Update() {
        if(this.attackTime > 0f) {
            this.updateAttackTime();
        }
        else {
            if(!this.isHitted)
                this.lookForPlayer();
        }

        if(this.isAttacking || this.isHitted)
            return;

        if(patrolPoints.Length > 1 && this.health > 0) {
            this.updatePatrolRoute();
            this.animator.Play("Walk");
        }
        else {
            this.animator.Play("Idle");
        }
    }

    // Public methpds
    public void Hit(int strength = 1) {
        this.health -= strength;
        this.isHitted = true;
        this.animator.Play("Hit");

        if(this.health <= 0) {
            this.Die();
        }
    }
    public void Die() {
        this.animator.Play("Die");
    }
    public void Attack() {
        this.animator.Play("Attack");
        this.attackTime = this.attackSpeed;
        this.isAttacking = true;
        this.player.HitByEnemy(this);
    }

    public int GetDirection() {
        return this.targetDirection;
    }
    public int GetFaceDirection() {
        return this.spriteRenderer.flipX ? -1 : 1;
    }
    public bool IsHitted() {
        return this.isHitted;
    }
    public bool IsDead() {
        return this.health <= 0;
    }

    // Private methods
    private void updateAttackTime() {
        this.attackTime -= Time.deltaTime;
    }
    private void updatePatrolRoute() {
        Vector3 target = patrolPoints[targetPoint].position;
        transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);

        if(this.spriteRenderer.flipX != (this.targetDirection == -1))
            this.spriteRenderer.flipX = this.targetDirection == -1;

        if(Vector2.Distance(target, transform.position) <= 0.1f) {
            transform.position = target;

            int nextPoint = targetPoint + targetDirection;
            if(nextPoint >= patrolPoints.Length)
                this.changeDirection(-1);
            else if(nextPoint < 0)
                this.changeDirection(1);

            targetPoint += targetDirection;
        }
    }
    private void lookForPlayer() {
        if(this.player && !this.player.IsDead() && this.behaviour == EnemyBehaviour.AGGRESSIVE) {
            float distance = Vector2.Distance(transform.position, player.transform.position);
            if(distance <= this.attackRange) {
                this.Attack();

                this.spriteRenderer.flipX = (this.player.transform.position - transform.position).x < 0;
            }
        }
    }

    private void changeDirection(int direction) {
        this.spriteRenderer.flipX = direction == -1;
        this.targetDirection = direction;
    }

    private void Ani_Hit() {
        this.isHitted = false;
    }
    private void Ani_Die() {
        Destroy(gameObject);
    }
    private void Ani_Attack() {
        this.isAttacking = false;
    }
}
